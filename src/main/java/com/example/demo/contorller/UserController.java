package com.example.demo.contorller;

import com.example.demo.dao.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @RequestMapping("setname")
    public String setname(String name){
        userService.setUser("ccc");

        System.out.println("设置用户名");
        return "setname.html";
    }

    @RequestMapping("getname")
    public String getname(){
        String name=userService.getUser();
        System.out.println(name);
        return "getname.html";
    }
}
